from __future__ import annotations

from math import log
from csv import DictReader
from typing import Any, Dict, List, Callable, Union
from collections.abc import Callable
import numpy as np
from ttictoc import tic, toc

"""
This script is an attempt to implement ID3 algorithm in the most data agnostic way possible

Links used for reference:

- ECSDI (UPC): Aprendizaje y Personalización (pgs 23 - 39) : https://www.cs.upc.edu/~bejar/ecsdi/Teoria/ECSDI07a-Personalizacion.pdf
- Step by Step Decision Tree: ID3 Algorithm From Scratch in Python [No Fancy Library]
https://medium.com/geekculture/step-by-step-decision-tree-id3-algorithm-from-scratch-in-python-no-fancy-library-4822bbfdd88f


Usage:
- Create a CSV file with some data (will contain the samples (X) and attributes (Ao) )
- Create a file (readable from the console < pipeline) with the following data (or insert manually as the program asks you):
  - Path to the file you created previously
  - The column char separator
  - The name of the field (attribute) that acts as the "class" (C in ID3(X,C,Af) )
  - Optionally, the fields that you don't want to be taken into account into Af (p.e, a row's ID) 

- Run the command python id3.py < input_param.in > output_result.out
"""


def getUniqueList(input_list: List[Any]):
    return list(np.unique(np.array(input_list)))


def get_value(x: Union[Dict[str, Any], object], key: str):
    if isinstance(x, dict):
        return x[key]
    elif isinstance(x, object):
        return getattr(x, key)
    else:
        raise ValueError("Wrong element of type {}. Can't read attribute.".format(type(x)))

def isEqual(item, attribute, value):
    return get_value(item, attribute) == value


def printTime(f):
    def wrapper(*args):
        tic()  # TicToc("name")
        v = f(*args)
        elapsed = toc()
        print('temps(s): {:.5f}'.format(elapsed))
        return v       
    return wrapper

# The data structure that contains the dataset and some useful operations
class Dataset:
    def __init__(self, header: List[str], items: List[Union[Dict[str, Any], object]]):
        
        self.header = header
        self._items = items
        self.n_rows = len(items)
        self.n_cols = len(header) 

    def items(self) -> List[Any]:
        return self._items      
        
    def items_by_filter(self, filter_function) -> List[Any]:
        filtered_items = [x for x in self._items if filter_function(x)]
        return filtered_items

    def count_by_filter(self, filter_function) -> int:
        return len(self.items_by_filter(filter_function))
    
    def sub_dataset_by_filter(self, filter_function) -> Dataset:
        return Dataset(self.header, self.items_by_filter(filter_function))

    def all_values_of(self, target_key: str) -> List[Any]:
        if target_key not in self.header:
            raise ValueError("Unknown key {} not found in dataset".format(target_key))           

        return getUniqueList(list(map(lambda x: get_value(x, target_key), self._items)))

    def size(self):
        return self.n_rows


# Contains the name of the class to use and its finite set of values
class Classifier:
    def __init__(self, label: str, dataset: Dataset):
        self._label = label
        self._class_list = dataset.all_values_of(label)

    def label(self) -> str:
        return self._label

    def class_list(self) -> List[Any]:
        return self._class_list


class Element:
    def __init__(self, name, values) -> None:
        self.name = name
        self.values = values

# The class containing the decision tree
class TreeElement(Element):
    def __init__(self, name: str, values: Dict[str, Union[TreeElement, Dict, List]], is_leaf=False) -> None:
        super().__init__(name, values)
        self.is_leaf = is_leaf

    def printTree(self, level=0):
        pText = self.name + "\n" if level == 0 else "{} |{} ({})\n".format('\t'*level, '-', self.name)
        print(pText)           
        if (self.is_leaf):
            print("{}{}\n".format('\t'*level, self.values))
        else:
            for key, elem in self.values.items():
                if (isinstance(elem, self.__class__)):
                    print("{}{}".format("\t"*(level+1), key))
                    elem.printTree(level+1)
                else:
                    print("-> {}".format(elem))


def i_x_c(x: Dataset, c: Classifier):
    # print("I(X,C)")          
    len_of_x = x.size()
    result = 0    
    for c_i in c.class_list():
        
        count_of_ci = x.count_by_filter(lambda x_i: isEqual(x_i, c.label(), c_i))       
        # print("P(X,ci) = |[ C(x) == {} ]|/|X| => {:.2f} ({}/{})".format(c_i, (count_of_ci/len_of_x), count_of_ci, len_of_x))
        if count_of_ci > 0:            
            p_ci = (count_of_ci/len_of_x)
            # print("total_ixc += {:.2f}".format((-p_ci*log(p_ci, 2))))
            result += (-p_ci*log(p_ci, 2))
    # print("I(X,C) = {:.2f}\n".format(result))
    return result


def e_x_a_c(x: Dataset, a: str, c: Classifier):
    print("\nAttribute: {}".format(a))
    print("|A| = {}".format(len(x.all_values_of(a))))
    # print("Class: {}".format(c.name))
    # print("|C| = {}".format(len(c.values)))    
    total_len_x = x.size()
    print("|X| = {}".format(total_len_x))
    result = 0
    for a_i in x.all_values_of(a):
        
        x_of_attribute_a_i = x.sub_dataset_by_filter(lambda x_i: isEqual(x_i, a, a_i))
        freq_ai = x_of_attribute_a_i.size()/total_len_x
        # print("total += (|[A(x) = {}]| / |X|) * I([A(x) = {}], C) => ".format(a_i, a_i))
        ixc_ai = i_x_c(x_of_attribute_a_i, c)
        # print("total += ({} / {}) * {} => total += {} * {} => total += {}".format(len(x_of_attribute_a_i), total_len_x, ixc_ai, freq_ai, ixc_ai, freq_ai*ixc_ai))
        result += freq_ai*ixc_ai

    print("E(X,A,C) = {:.2f}".format(result))
    return result


# x -> The Dataset of samples
# c -> 
def id3(x: Dataset, c: Classifier, a: List[str]):
    # Root case: all elements belong to the same class (only 1 value of c.class_list appears in the elements of x)
    unique_c_values = x.all_values_of(c.label())
    if len(unique_c_values) == 1:        
        return TreeElement(unique_c_values[0], x.items(), True)

    # Recursive case: find the next attribute with greatest entropy gain, create a root node for it and make a branch for each of its values
    node_i_x_c = i_x_c(x, c)
    print("New Initial I = {:.2f}".format(node_i_x_c))
    maxG = 0
    next_attribute = None 
    print("Searching Attribute with best gain G(X,a)")
    for a_i in a:        
        old_max = maxG
        entropy_for_a_i = e_x_a_c(x, a_i, c)
        entropy_gain = node_i_x_c - entropy_for_a_i
        print("G(X,{}) = {:.2f} - {:.2f} = {:.2f}".format(a_i, node_i_x_c, entropy_for_a_i, entropy_gain))
        maxG = max(old_max, entropy_gain)
        if maxG != old_max:
            next_attribute = a_i
    print("Next important attribute is {}".format(next_attribute))
    new_a = [a_z for a_z in a if a_z != next_attribute]
    print("Creating branches for node with name {}\n".format(next_attribute))

    node_branches = {v_i: id3(
        x.sub_dataset_by_filter(lambda x_i: isEqual(x_i, next_attribute, v_i)),
        c,
        new_a
        ) for v_i in x.all_values_of(next_attribute)}
    return TreeElement(next_attribute, node_branches) 


@printTime
def run_idx(x, c, a):
    return id3(x, c, a)

class_values = []
samples = []

target_csv_filename = input('Where is the dataset (only supports CSV files for now) ?: \n')
csv_delimiter = input("Set column delimiter: \n")

with open(target_csv_filename, 'r', encoding='utf8', newline='') as csvfile:
    original_samples = DictReader(csvfile, delimiter=csv_delimiter)    

    className = input("Found attributes: {}\nWhich one acts as classifier ?: \n")
    if className not in original_samples.fieldnames:
        raise AttributeError("{} is not present in file fields".format(className))

    

    restricted_attributes = input("Which attributes must be ignored (separated by comma) ?").strip().split(',')
    restricted_attributes += [className]
    attributes = [a for a in original_samples.fieldnames if a not in restricted_attributes]
    
    for row in original_samples:
        samples.append(row)
    
    dataset_x = Dataset(original_samples.fieldnames, samples)
    dataClassifier = Classifier(className, dataset_x)   

    decissiontree = run_idx(dataset_x, dataClassifier, attributes)
    decissiontree.printTree()
    


