# AI related algorithms

This section collects a bunch of scripts that I make in order to understand, to certain degree, how the most widely used algorithms in AI fields work.

## Roadmap

* [ ] Decision Tree Algorithms
   * [x] Basic ID3 
   * [ ] C4.5
   * [ ] CHAID
   * [ ] MARS
   * [ ] CART
* [ ] Bayesan Networks
   * [ ] Naive Bayesan
   * [ ] Variable filtering algorithm
* [ ] Neural Networks
* [ ] Probabilistic models
  

## Author
Daniel Garrapucho Lévy

## License
GNU GENERAL PUBLIC LICENSE, Version 3

## Project status
WIP
